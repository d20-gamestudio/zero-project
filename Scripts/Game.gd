extends Node2D

const PLAYER = preload("res://Scenes/Player.tscn")
onready var Walls = get_parent().get_node("Walls")
onready var Player = get_parent().get_node("Player")
var PLAYER_INIT_POS
export var tab_num = 3 #Colocar o número de tabs da fase
var comand_order = [[],[],[]]
var vari_slots = [] 
var tab = 0
var comand_quant = [-1,-1,-1]
var player_num = -1
var play = false
var last_slot_destroy = 0
var vari_numbers =[[1.0, 1.5, 2.0], [0, 1, 2]]
var vari_lv = 0
var vari_speed_i = 0
var vari_color_i = 0
var loop_time = 1
var loop_list = [[],[],[]]
var touch_pos
var slot_num = 16
var slider = true
var click_ok = true
var finalNum
var portalNum

# Called when the node enters the scene tree for the first time.
func _ready():
	finalNum = get_parent().get_node("Blocks/Finals").get_child_count()
	portalNum = get_parent().get_node("Blocks/Portals").get_child_count()
	
	get_parent().get_node("Data/Pause").visible = false
	get_node("Commands").get_node("Tab0").position.y = 1384
	
	for i in range(slot_num):
		get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E47240")
	
	if get_node("Commands/Blocks/Variable").visible == true:
		get_node("Commands/Blocks/Variable").self_modulate = Color("#E47240")
	
	PLAYER_INIT_POS = Player.position
	
	for j in range(tab_num):
		for i in range(slot_num):
			comand_order[j].append(0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

#controla a ordem de comados do lado esquerdo
func comand_order_creation(id, direct):
	if id == 7 && id < comand_quant[tab] && direct == 6:
		if comand_order[tab][id+1] == 6:
			comand_order_creation(id+3, direct)
		else:
			comand_order_creation(id+2, direct)
	elif comand_order[tab][id] == 0:
		comand_order[tab][id] = direct
		get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").frame = direct
		get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#FFFFFF")
		if direct == 6:
			get_node("Commands/Slot" + str(id)).position.x += 46
			get_node("Commands/Slot" + str(id + 1)).visible = false
			get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").visible = true
			get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").frame = vari_speed_i
			var v = [vari_numbers[0][vari_speed_i],vari_numbers[1][vari_color_i]]
			comand_order[tab][id + 1] = v
			comand_quant[tab] += 1
			match (v[1]):
				0:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E47240")
				1:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#F4DE67")
				2:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E03132")
		elif direct == 7:
			loop_list[tab].append([loop_time, 0, id])
		if comand_quant[tab] < slot_num - 1:
			comand_quant[tab] += 1
	elif id < slot_num - 1:
		if comand_order[tab][id] == 6:
			comand_order_creation(id+2, direct)
		else:
			comand_order_creation(id+1, direct)

#é chamada para retirar algum comando
func comand_order_destroy(id,original):
	if typeof(comand_order[tab][id]) != TYPE_ARRAY:
		if original == true:
			last_slot_destroy = id
			if comand_order[tab][id] == 6:
				vari_slots.append(id)
		if comand_order[tab][id] == 7:
			loop_list[tab].remove(loop_list[tab].size() - 1)
	if id < slot_num - 1:
		
		if id == 7 && typeof(comand_order[tab][id+1]) != TYPE_ARRAY && comand_order[tab][id+1] == 6:
			comand_order[tab][id] = 0
			vari_slots.append(id+1)
		elif id == 6 && typeof(comand_order[tab][id+1]) != TYPE_ARRAY && comand_order[tab][id+1] == 0 && typeof(comand_order[tab][id+2]) != TYPE_ARRAY && comand_order[tab][id+2] == 6:
				comand_order[tab][id] = comand_order[tab][id+2]
				comand_order[tab][id+1] = comand_order[tab][id+3]
				comand_order_destroy(id+2,original)
		else:
			comand_order[tab][id] = comand_order[tab][id+1]
			if typeof(comand_order[tab][id]) == TYPE_ARRAY || comand_order[tab][id+1] != 0:
				comand_order_destroy(id+1,original)
	elif id == slot_num - 1:
		comand_order[tab][id] = 0

func slot_visual_change(id, vari_id):
	if vari_slots.size() != 0 and vari_slots[vari_id] == id:
			if vari_id < vari_slots.size() - 1:
				vari_id += 1
			if typeof(comand_order[tab][id]) != TYPE_ARRAY and comand_order[tab][id] == 6:
				var v
				match comand_order[tab][id+1][0]:
					1.0:
						v = 0
					1.5:
						v = 1
					2.0:
						v = 2
				get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").frame = v
			else:
				get_node("Commands/Slot" + str(id)).position.x -= 46
				get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").visible = false
				get_node("Commands/Slot" + str(id + 1)).visible = true
	else:
		if typeof(comand_order[tab][id]) != TYPE_ARRAY and comand_order[tab][id] == 6:
			get_node("Commands/Slot" + str(id)).position.x += 46
			get_node("Commands/Slot" + str(id + 1)).visible = false
			get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").visible = true
			var v
			match comand_order[tab][id+1][0]:
				1.0:
					v = 0
				1.5:
					v = 1
				2.0:
					v = 2
			get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").frame = v
	if typeof(comand_order[tab][id]) != TYPE_ARRAY:
		get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").frame = comand_order[tab][id]
		if comand_order[tab][id] == 0:
			match (tab):
				0:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E47240")
				1:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#F4DE67")
				2:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E03132")
		elif comand_order[tab][id] == 6:
			match (comand_order[tab][id+1][1]):
				0:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E47240")
				1:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#F4DE67")
				2:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E03132")
		else:
			get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#FFFFFF")
	else:
		get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").frame = 0
		match (tab):
				0:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E47240")
				1:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#F4DE67")
				2:
					get_node("Commands").get_node("Slot"+ str(id) + "/AnimatedSprite").modulate = Color("#E03132")
	if id < slot_num - 1 && id < last_slot_destroy + 3:
		slot_visual_change(id+1,vari_id)
	else:
		vari_slots = []


#serve para trocar as abas coloridas
func tab_change(id):
	var variables = []
	if get_node("Commands/Blocks/Variable").visible == true:
		match(id):
			0:
				get_node("Commands/Blocks/Variable").self_modulate = Color("#E47240")
			1:
				get_node("Commands/Blocks/Variable").self_modulate = Color("#F4DE67")
			2:
				get_node("Commands/Blocks/Variable").self_modulate = Color("#E03132")
		
		for i in range(comand_quant[tab] +1):
			if typeof(comand_order[tab][i]) != TYPE_ARRAY && comand_order[tab][i] == 6:
				variables.append(i)
	
	
	
	for i in range(tab_num):
		if i == id:
			get_node("Commands").get_node("Tab"+ str(i)).position.y = 1384
			tab = i
		else:
			get_node("Commands").get_node("Tab"+ str(i)).position.y = 1409
	
	
	var variables_i = 0
	for id in range(slot_num):
		if variables.size() != 0 and variables[variables_i] == id:
			if variables_i < variables.size() - 1:
				variables_i += 1
			if typeof(comand_order[tab][id]) != TYPE_ARRAY and comand_order[tab][id] == 6:
				var v
				match comand_order[tab][id+1][0]:
					1.0:
						v = 0
					1.5:
						v = 1
					2.0:
						v = 2
				get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").frame = v
			else:
				get_node("Commands/Slot" + str(id)).position.x -= 46
				get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").visible = false
				get_node("Commands/Slot" + str(id + 1)).visible = true
		else:
			if typeof(comand_order[tab][id]) != TYPE_ARRAY and comand_order[tab][id] == 6:
				get_node("Commands/Slot" + str(id)).position.x += 46
				get_node("Commands/Slot" + str(id + 1)).visible = false
				get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").visible = true
				var v
				match comand_order[tab][id+1][0]:
					1.0:
						v = 0
					1.5:
						v = 1
					2.0:
						v = 2
				get_node("Commands/Slot" + str(id)+"/AnimatedSprite/AnimatedSprite").frame = v
		
	for i in range(slot_num):
		if typeof(comand_order[tab][i]) != TYPE_ARRAY:
			get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").frame = comand_order[tab][i]
			if comand_order[tab][i] == 0:
				match (id):
					0:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E47240")
					1:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#F4DE67")
					2:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E03132")
			elif comand_order[tab][i] == 6:
				match (comand_order[tab][i+1][1]):
					0:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E47240")
					1:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#F4DE67")
					2:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E03132")
			else:
				get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#FFFFFF")
		else:
			get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").frame = 0
			match (id):
					0:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E47240")
					1:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#F4DE67")
					2:
						get_node("Commands").get_node("Slot"+ str(i) + "/AnimatedSprite").modulate = Color("#E03132")
#criar os player de diferentes cores e passar o movimento para eles
func player_spawn(pos, no_direction, tab):
	if tab > player_num && tab < tab_num - 1:
		var curr_tile = Walls.world_to_map(pos)
		var player = PLAYER.instance()
		player.position = pos
		player.color = tab + 1
		player.tab = tab + 1
		player.no_direc = no_direction
		get_parent().add_child(player)
		player_num += 1
	#next_player()

func next_player():
	tab += 1
	tab_change(tab)


#as seguintes funções definem o que cada botão faz:
func _on_Left_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		comand_order_creation(0, 1)
	
func _on_Right_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		comand_order_creation(0, 2)

func _on_Run_released():
	#É AQUI POHA
	if play == false && click_ok == true:
		#var has_start = false
		for i in range (slot_num):
			#if has_start == false:
			if typeof(comand_order[0][i]) != TYPE_ARRAY:
				if comand_order[0][i] == 4:
					if i < slot_num - 1:
						Player.comandID = i + 1
							#has_start = true
		#var has_final = false
		#var double_start = false
		#var double_final = false
		#if has_start == true:
			#for i in range (Player.comandID, slot_num - 1):
				#if typeof(comand_order[0][i]) != TYPE_ARRAY:
					#if comand_order[0][i] == 5:
						#has_final = true
						#for j in range (i, slot_num - 1):
							#if comand_order[0][i] == 5:
								#double_final = true
								#j = slot_num
							#elif comand_order[0][i] == 4:
								#double_start = true
								#j = slot_num
					#elif comand_order[0][i] == 4:
						#double_start = true
						#i = slot_num
		#if has_start == true && has_final == true && double_final == false && double_start == false:
		Player.start = true
		play = true
		$Commands/Run.self_modulate = "#0367A3"
		$Commands/Run/Label.text = "Reset"
		#else:
			#$Ui/AlertPopUp.visible = true
	else:
		$Commands/Run.self_modulate = "#5aac1e"
		$Commands/Run/Label.text = "Run"
		get_parent().get_node("Data").set_receptorActivates()
		Walls.player_path.append(Walls.world_to_map(Player.position))
		for i in range (player_num + 1):
			Walls.player_path.append(Walls.world_to_map(get_parent().get_child(i + 6).position))
			get_parent().get_child(i + 6).queue_free()
		Player.comandID = 0
		player_num = -1
		play = false
		Player.position = PLAYER_INIT_POS
		for i in range (Walls.player_path.size()):
			Walls.set_cellv(Walls.player_path[i], 1)
		Player.collisionOk = true
		Player.color = 0
		Player.get_node("Sprite").frame = 0
		Walls.player_path = []
		Player.set_direc = true
		Walls.set_cellv(Walls.world_to_map(Player.position), Player.color + 2)
		Player.player_change = false
		Player.mov_change = false
		Player.get_node("ChangeMovTimer").stop()
		for i in range(finalNum):
			if get_parent().get_node("Blocks/Finals").get_child(i).color != get_parent().get_node("Blocks/Finals").get_child(i).original_color:
				get_parent().get_node("Blocks/Finals").get_child(i).change_color(get_parent().get_node("Blocks/Finals").get_child(i).original_color)
			if get_parent().get_node("Blocks/Finals").get_child(i).blocked != get_parent().get_node("Blocks/Finals").get_child(i).original_lock:
				get_parent().get_node("Blocks/Finals").get_child(i).lock()
		for i in range(portalNum):
			get_parent().get_node("Blocks/Portals").get_child(i).reset()

func _on_Tab0_released():
		if tab != 0 && tab_num > 0 && click_ok == true:
			tab_change(0)


func _on_Tab1_released():
		if tab != 1 && tab_num > 1 && click_ok == true:
			tab_change(1)


func _on_Tab2_released():
		if tab != 2 && tab_num > 2 && click_ok == true:
			tab_change(2)




func _on_Pause_pressed():
	AudioController.selected()
	get_parent().get_node("Data/Pause").visible = true
	get_tree().paused = true


func _on_MoveOn_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		comand_order_creation(0, 3)


func _on_Start_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		comand_order_creation(0, 4)

func _on_Stop_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		comand_order_creation(0, 5)


func _on_Variable_released():
	if play == false && comand_quant[tab] < slot_num - 2 && click_ok == true:
		if comand_quant[tab] != 6:
			slider = false
			vari_lv = 0
			vari_speed_i = 0
			vari_color_i = tab
			$Commands/Blocks/Variable/Var_popup/Title.text = "Velocidade"
			$Commands/Blocks/Variable/Var_popup/Number.visible = true
			$Commands/Blocks/Variable/Var_popup/Number.text = "1.0"
			$Commands/Blocks/Variable/Var_popup/ColorRect.visible = false
			if $Commands/Blocks/Variable/Var_popup.visible == false:
				$Commands/Blocks/Variable/Var_popup.visible = true
			else:
				$Commands/Blocks/Variable/Var_popup.visible = false


func _on_Speed_left_released():
	if vari_lv == 0:
		if vari_speed_i > 0:
			vari_speed_i -= 1
		else:
			vari_speed_i = 2
		match(vari_speed_i):
			0:
				$Commands/Blocks/Variable/Var_popup/Number.text = "1.0"
			1:
				$Commands/Blocks/Variable/Var_popup/Number.text = "1.5"
			2:
				$Commands/Blocks/Variable/Var_popup/Number.text = "2.0"
		
	else:
		if vari_color_i > 0:
			vari_color_i -= 1
		else:
			vari_color_i = 2
		match vari_color_i:
			0:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("E47240")
			1:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("F4DE67")
			2:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("E03132")


func _on_Speed_right_released():
	if vari_lv == 0:
		if vari_speed_i < 2:
			vari_speed_i += 1
		else:
			vari_speed_i = 0
		match(vari_speed_i):
			0:
				$Commands/Blocks/Variable/Var_popup/Number.text = "1.0"
			1:
				$Commands/Blocks/Variable/Var_popup/Number.text = "1.5"
			2:
				$Commands/Blocks/Variable/Var_popup/Number.text = "2.0"
	else:
		if vari_color_i < 2:
			vari_color_i += 1
		else:
			vari_color_i = 0
		match vari_color_i:
			0:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("E47240")
			1:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("F4DE67")
			2:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("E03132")



func _on_Confirm_released():
	if vari_lv == 0:
		vari_lv += 1
		$Commands/Blocks/Variable/Var_popup/Title.text = "Cor"
		$Commands/Blocks/Variable/Var_popup/Number.visible = false
		$Commands/Blocks/Variable/Var_popup/ColorRect.visible = true
		match vari_color_i:
			0:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("E47240")
			1:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("F4DE67")
			2:
				$Commands/Blocks/Variable/Var_popup/ColorRect.color = Color("E03132")
	else:
		if play == false && comand_quant[tab] < slot_num - 2:
			click_ok = true
			slider = true
			comand_order_creation(0, 6)
			$Commands/Blocks/Variable/Var_popup.visible = false



func _on_LoopStart_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		click_ok = false
		slider = false
		loop_time = 1
		if $Commands/Loop_popup.visible == false:
			$Commands/Loop_popup.visible = true
		else:
			$Commands/Loop_popup.visible = false


func _on_LoopEnd_released():
	if play == false && comand_quant[tab] < slot_num - 1 && click_ok == true:
		comand_order_creation(0, 8)


func _on_Loop_left_released():
	if loop_time > 1:
		loop_time -= 1
	else:
		loop_time = 5


func _on_Loop_right_released():
	if loop_time < 5:
		loop_time += 1
	else:
		loop_time = 1

#Deslizar dos nodes
func _on_Loop_confirm_released():
	if play == false && comand_quant[tab] < slot_num - 1:
		click_ok = true
		slider = true
		comand_order_creation(0, 7)


#deslizar os blocos na tela
func _input(event):
	if slider == true:
		if event is InputEventScreenTouch:
			if event.pressed == false:
				click_ok = true
			touch_pos = event.position.x
		if event.position.y > 1698 and event.position.y < 1838:
			if event is InputEventScreenDrag:
				click_ok = false
				if touch_pos - event.position.x > 0 and $Commands/Blocks.position.x > 177:
					$Commands/Blocks.position.x -= 15
					touch_pos = event.position.x
				elif touch_pos - event.position.x < 0 and $Commands/Blocks.position.x < 560:
					$Commands/Blocks.position.x += 15
					touch_pos = event.position.x
					

func set_visibleBlocks(numb):
	match numb:
		1:
			$Commands/Blocks/Variable.visible = false
			$Commands/Blocks/LoopStart.visible = false
			$Commands/Blocks/LoopEnd.visible = false
		2: 
			$Commands/Blocks/LoopStart.visible = false
			$Commands/Blocks/LoopEnd.visible = false

func set_visibleTabs(numbT):
	match numbT:
		1:
			$Commands/Tab1.visible = false
			$Commands/Tab2.visible = false
		2:
			$Commands/Tab2.visible = false

func set_FaseText():
	$Ui/UI/FaseText.text = Save.data["nivel_atual"]


func slot_touch(SlotID):
	if play == false && typeof(comand_order[tab][SlotID]) != TYPE_ARRAY && click_ok == true:
		if comand_order[tab][SlotID] != 0:
			if comand_order[tab][SlotID] == 6:
				comand_quant[tab] -= 2
			else:
				comand_quant[tab] -= 1
			comand_order_destroy(SlotID, true)
			if typeof(comand_order[tab][SlotID]) == TYPE_ARRAY:
					comand_order_destroy(SlotID, false)
			if typeof(comand_order[tab][7]) == TYPE_ARRAY && typeof(comand_order[tab][8]) == TYPE_ARRAY:
					comand_order_destroy(8, false)
			slot_visual_change(SlotID, 0)


func _on_Slot0_released():
	slot_touch(0)

func _on_Slot1_released():
	slot_touch(1)


func _on_Slot2_released():
	slot_touch(2)


func _on_Slot3_released():
	slot_touch(3)


func _on_Slot4_released():
	slot_touch(4)


func _on_Slot5_released():
	slot_touch(5)


func _on_Slot6_released():
	slot_touch(6)


func _on_Slot7_released():
	slot_touch(7)


func _on_Slot8_released():
	slot_touch(8)


func _on_Slot9_released():
	slot_touch(9)


func _on_Slot10_released():
	slot_touch(10)


func _on_Slot11_released():
	slot_touch(11)


func _on_Slot12_released():
	slot_touch(12)


func _on_Slot13_released():
	slot_touch(13)


func _on_Slot14_released():
	slot_touch(14)


func _on_Slot15_released():
	slot_touch(15)


func _on_TouchScreenButton_released():
	$Ui/AlertPopUp.visible = false
