extends Node2D



# Called when the node enters the scene tree for the first time.
func _ready():
	Save.load_data()
	$Texts/LastFase.text = "run."+ Save.data["nivel_atual"]


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

#change scenes by click on buttons
func _on_FasesIcon_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Fases.tscn")


func _on_DocIcon_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Repository.tscn")


func _on_DesempenhoIcon_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Performance.tscn")


func _on_Continue_pressed():
	AudioController.Confirm()
	$Blink.play("transition")


func _changeScene():
	var last_scene = "res://Scenes/Fase" + Save.data["nivel_atual"] + ".tscn"
	get_tree().change_scene(last_scene)


func _on_CredIcon_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Credits.tscn")
