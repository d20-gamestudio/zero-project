extends Area2D


var id = 1

export var color = 0
export var num = -1
export var blocked = false
var unlock_animation = false
var original_color
var original_lock

# Called when the node enters the scene tree for the first time.
func _ready():
	$Sprite.frame = color
	if blocked == true:
		$Block.visible = true
	original_color = color
	original_lock = blocked


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if unlock_animation == true:
		$Block.modulate.a -= 0.05
		if $Block.modulate.a <= 0:
			unlock_animation = false

func unlock():
	blocked = false
	$Block.frame = 1
	unlock_animation = true
	
func lock():
	blocked = true
	$Block.frame = 0
	unlock_animation = false
	$Block.modulate.a = 1

func change_color(new_color):
	color = new_color
	$Sprite.frame = color
	
