extends Node2D


onready var walls = get_parent().get_node("Walls")
onready var game = get_parent().get_node("Buttons")
var no_direc = -1
var collisionOk = false
var id = 0
var tab = 0
var color = 0
var colors = [Color("#E47240"), Color("#F4DE67"), Color("#E03132")]
var direction = [Vector2(0,-1),Vector2(1,0),Vector2(0,1), Vector2(-1,0)]
var directionID
var cur_direct
var player_change = false
var mov_change = false
var pos_to_move = position
var color_pos = Vector2(0,0)
var set_direc = true
var comandID = 0
var start = false
var speedx = 1.0
const SPEED = 10
var loop_i = -1
var tile_size = 16


# Called when the node enters the scene tree for the first time.
func _ready():
	$spawnTimer.start()
	$Sprite.frame = color
	walls.set_cellv(walls.world_to_map(position), color + 2)
	if tab > 0:
		for i in range (game.slot_num):
			if typeof(game.comand_order[tab][i]) != TYPE_ARRAY && game.comand_order[tab][i] == 4:
				start = true
				if i < game.slot_num - 1:
					comandID = i + 1
				i = game.slot_num
	else:
		collisionOk = true


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#movimentação e direcionamento do player:
	if game.play == true && $spawnTimer.get_time_left() == 0 && start == true:
		if set_direc == true:
			directionID = walls.set_direction(position, no_direc)
			direction[directionID]
			cur_direct = direction[directionID]
			no_direc = -1
			set_direc = false
		if mov_change == true && $ChangeMovTimer.get_time_left() == 0:
			no_move_blocks()
			if start == true:
				if game.comand_order[tab][comandID] == 2:
					if directionID < 3:
						directionID += 1
					else:
						directionID = 0
				elif game.comand_order[tab][comandID] == 1:
					if directionID > 0:
						directionID -= 1
					else:
						directionID = 3
				
				cur_direct = direction[directionID]
				if comandID < game.slot_num - 1:
					comandID += 1
			mov_change = false
		if mov_change == false:
			var actual_pos = pos_to_move
			if mov_change == false:
				pos_to_move = walls.is_tile_vacant(position, cur_direct)
			if pos_to_move != actual_pos:
				walls.player_path.append(walls.world_to_map(position))
				color_pos = (pos_to_move + actual_pos) / 2
				
			if cur_direct != Vector2(0,0) and mov_change == false:
				if abs(color_pos.x - position.x) < tile_size + tile_size/2 && abs(color_pos.y - position.y) < tile_size + tile_size/2:
					if walls.get_cellv(walls.world_to_map(position)) >= 01: 
						walls.set_cellv(walls.world_to_map(position), color + 2)
				position = position.linear_interpolate(pos_to_move, speedx * SPEED * delta)
			

func no_move_blocks():
	if game.comand_order[tab][comandID] == 0 || game.comand_order[tab][comandID] == 5:
		start = false
		cur_direct = Vector2(0,0)
	elif game.comand_order[tab][comandID] == 6:
		speedx = game.comand_order[tab][comandID+1][0]
		color = game.comand_order[tab][comandID+1][1]
		$Sprite.frame = color
		if comandID < game.slot_num - 2:
			comandID += 2
			no_move_blocks()
	elif game.comand_order[tab][comandID] == 7:
		var new_loop = true
		for i in range(game.loop_list[tab].size()):
			if comandID == game.loop_list[tab][i][2]:
				new_loop = false
		if new_loop == true:
			loop_i += 1
		if comandID < game.slot_num - 1:
			comandID += 1
			no_move_blocks()
	elif game.comand_order[tab][comandID] == 8:
		loop_verification(loop_i)
		

func loop_verification(lop_i):
	if game.loop_list[tab][lop_i][0] != game.loop_list[tab][lop_i][1]:
		game.loop_list[tab][lop_i][1] += 1
		comandID = game.loop_list[tab][lop_i][2] + 1
		no_move_blocks()
	elif lop_i > 0:
		loop_verification(lop_i - 1)

func next_direction(id):
	if game.comand_order[tab][id] == 6:
		return next_direction(id+2)
	elif game.comand_order[tab][id] == 7:
		return next_direction(id+1)
	elif game.comand_order[tab][id] == 8:
		return next_loop(loop_i)
	else:
		return id
func next_loop(loopId):
	if game.loop_list[tab][loopId][0] != game.loop_list[tab][loopId][1]:
		return next_direction(game.loop_list[tab][loopId][2] + 1)
	elif loopId > 0:
		return next_loop(loopId - 1)
		
func final_check():
	if game.comand_order[tab][comandID] != 0 && start == true:
		if game.comand_order[tab][comandID] == 5:
			start = false
			cur_direct = Vector2(0,0)
		elif game.comand_order[tab][comandID] == 6:
			if comandID < game.slot_num - 2:
				comandID += 2
				final_check()
		elif comandID < game.slot_num - 1:
			comandID += 1
			final_check()

#colisão do player com os blocos ao longo da fase:
func _on_Player_area_entered(area):
	
	if collisionOk == false:
		if area.id == 2:
			if area.divide == true:
					collisionOk = true
	else:
		if area.id == 2:
			if area.portal == true:
				area.get_node("Sprite").self_modulate = colors[color]
				var enter_arrow
				var arrow_rotation
				match(directionID):
					0:
						enter_arrow = 2
						arrow_rotation = -90
					1:
						enter_arrow = 3
						arrow_rotation = 0
					2:
						enter_arrow = 0
						arrow_rotation = 90
					3:
						enter_arrow = 1
						arrow_rotation = -180
				if area.direction[enter_arrow].visible == true:
					#area.get_node("Sprite").self_modulate = colors[color]
					area.direction[enter_arrow].self_modulate = colors[color]
					area.direction[enter_arrow].rotation_degrees = arrow_rotation
				var out_arrow
				var i = comandID
				while out_arrow == null:
					if typeof(game.comand_order[tab][i]) != TYPE_ARRAY && game.comand_order[tab][i] != 0:
						if game.comand_order[tab][i] < 4:
							match(game.comand_order[tab][i]):
								1:
									if directionID == 0:
										out_arrow = 3
									else:
										out_arrow = directionID - 1
								2:
									if directionID == 3:
										out_arrow = 0
									else:
										out_arrow = directionID + 1
								3:
									out_arrow = directionID
						elif game.comand_order[tab][i] == 6:
							i += 2
						else:
							i += 1
					elif typeof(game.comand_order[tab][i]) == TYPE_ARRAY:
						if i < game.comand_quant[tab]:
							i += 1
					else:
						out_arrow = -1
				if out_arrow >= 0 && area.direction[out_arrow].visible == true:
					#area.get_node("Sprite").self_modulate = colors[color]
					if typeof(game.comand_order[tab][comandID]) != TYPE_ARRAY && game.comand_order[tab][comandID] == 6:
						area.direction[out_arrow].self_modulate = colors[game.comand_order[tab][comandID+1][1]]
					else:
						area.direction[out_arrow].self_modulate = colors[color]
					area.direction[out_arrow].rotation_degrees = area.rotation_d[out_arrow]
				$ChangeMovTimer.start()
				mov_change = true
				position = area.position
			if area.divide == true:
				if tab > game.player_num && tab < game.tab_num - 1:
					var next_no_direct
					if start == false:
						next_no_direct = -1
					else:
						var id = next_direction(comandID)
						if game.comand_order[tab][id] == 2:
							if directionID < 3:
								next_no_direct = directionID + 1
							else:
								next_no_direct = 0
						elif game.comand_order[tab][id] == 1:
							if directionID > 0:
								next_no_direct = directionID - 1
							else:
								next_no_direct = 3
						elif game.comand_order[tab][id] == 3:
							next_no_direct = directionID
					var next_direct = walls.set_direction(position, next_no_direct)
					if walls.set_direction(area.position, next_no_direct) != null:
						for i in range (game.slot_num):
							if typeof(game.comand_order[tab+1][i]) != TYPE_ARRAY && game.comand_order[tab+1][i] == 4:
								area.direction[next_direct].rotation_degrees = area.rotation_d[next_direct]
								area.direction[next_direct].self_modulate = colors[color+1]
								i = game.slot_num
						game.player_spawn(area.position, next_no_direct, tab)
		elif area.id == 1:
			final_check()
			if color == area.color and area.blocked == false and start == false:
				area.get_node("Confirm").modulate = colors[color]
				get_parent().get_node("Data")._Win()
		elif area.id == 3:
			var finalNum = get_parent().get_node("Blocks/Finals").get_child_count()
			if area.key == true:
				for i in range(finalNum):
					if get_parent().get_node("Blocks/Finals").get_child(i).num == area.blocked_final:
						get_parent().get_node("Blocks/Finals").get_child(i).unlock()
			else:
				for i in range(finalNum):
					if get_parent().get_node("Blocks/Finals").get_child(i).color == area.arrowL:
						get_parent().get_node("Blocks/Finals").get_child(i).change_color(area.arrowR)
					elif get_parent().get_node("Blocks/Finals").get_child(i).color == area.arrowR:
						get_parent().get_node("Blocks/Finals").get_child(i).change_color(area.arrowL)
					



