extends Node


var music_control = 1
var volume_on = 2
var pause: TextureButton
var music_playing = false


#func _ready():
#	pass
	
	
func _process(delta):
	
	if music_control == 1 and $Music1.is_playing() == false:
		$Music2.play()
		music_control = 2
	elif music_control == 2 and $Music2.is_playing() == false:
		$Music1.play()
		music_control = 1
	
	

func selected():
	$Button_selected.play()

func Confirm():
	$Confirm.play()


func blocked():
	$Blocked.play()

func Writing():
	$Writing.play()

func StopWriting():
	print("Não devo estar tocando")
	$Writing.stop()


func ZoomIn():
	$ZoomIn.play()

func ZoomOut():
	$ZoomOut.play()

func volume():
	if volume_on == 1:
		set_musicPlaying(1)
		$Music1.volume_db = -20
		$Music2.volume_db = -20
		volume_on = 2
	else:
		set_musicPlaying(2)
		$Music1.volume_db = -80
		$Music2.volume_db = -80
		volume_on = 1
		
func get_musicPlaying():
	return music_playing

func set_musicPlaying(numb):
	if numb == 1:
		music_playing = false
	else:
		music_playing = true
