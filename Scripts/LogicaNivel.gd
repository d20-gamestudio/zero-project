extends Node2D

var touch_pos
var scene
var On = "#02a974"
var Off = "#bb2222"
onready var Confirm = load("res://Assets/Hud/Nivel/Confirm.png")
#onready var 

func _ready():
	$TopSide/Sound.pressed = AudioController.get_musicPlaying()
	print(Save.data["prox_nivel"])
	pathGreen()
	$Alert.visible = false
	$TopSide/BlackScreem.visible = false

func _transtion():
	match scene:
		1: 
			get_tree().change_scene("res://Scenes/Fase1-1.tscn")
		2:
			get_tree().change_scene("res://Scenes/Fase1-2.tscn")
		3:
			get_tree().change_scene("res://Scenes/Fase1-3.tscn")
		4:
			get_tree().change_scene("res://Scenes/Fase1-4.tscn")
		5:
			get_tree().change_scene("res://Scenes/Fase1-5.tscn")
		6:
			get_tree().change_scene("res://Scenes/Fase1-6.tscn")
	return


#pintar os elementos de verde
func pathGreen():
	if Save.data["Fase1"]["first_anim"] == false and Save.data["Fase1"]["nivel2"]["completo"] == false: 
		$AnimationPlayer.play("Trans1-2")
		Save.data["Fase1"]["first_anim"] = true
		Save.save_data()
	else:
		$Buttons/Node2D/GuideLines/GuideLIne.default_color = On
		$Buttons/Node2D/Alert2.texture = Confirm
		get_node("Buttons/Nivel1-2").self_modulate = On
		get_node("Buttons/Nivel1-2").disabled = false
		
	if Save.data["Fase1"]["nivel2"]["completo"]:
		$Buttons/Node2D/GuideLines/GuideLIne2.default_color = On
		$Buttons/Node2D/Alert3.texture = Confirm
		get_node("Buttons/Nivel1-3").self_modulate = On
		get_node("Buttons/Nivel1-3").disabled = false
		
	if Save.data["Fase1"]["nivel3"]["completo"]:
		$Buttons/Node2D/GuideLines/GuideLIne3.default_color = On
		$Buttons/Node2D/Alert4.texture = Confirm
		get_node("Buttons/Nivel1-4").self_modulate = On
		get_node("Buttons/Nivel1-4").disabled = false
		
	if Save.data["Fase1"]["nivel4"]["completo"]:
		$Buttons/Node2D/GuideLines/GuideLIne5.default_color = On
		$Buttons/Node2D/Alert5.texture = Confirm
		get_node("Buttons/Nivel1-5").self_modulate = On
		get_node("Buttons/Nivel1-5").disabled = false

	if Save.data["Fase1"]["nivel5"]["completo"]:
		$Buttons/Node2D/GuideLines/GuideLIne6.default_color = On
		$Buttons/Node2D/GuideLines/GuideLIne7.default_color = On
		$Buttons/Node2D/Alert6.texture = Confirm
		get_node("Buttons/Nivel1-6").self_modulate = On
		get_node("Buttons/Nivel1-6").disabled = false
	stars()
	
#exibir pontuação das fases
func stars():
	print("A fase 1 tem: " + str(Save.data["Fase1"]["nivel1"]["estrelas"]))
	print("A fase 2 tem: " + str(Save.data["Fase1"]["nivel2"]["estrelas"]))
	print("A fase 3 tem: " + str(Save.data["Fase1"]["nivel3"]["estrelas"]))
	print("A fase 4 tem: " + str(Save.data["Fase1"]["nivel4"]["estrelas"]))
	print("A fase 5 tem: " + str(Save.data["Fase1"]["nivel5"]["estrelas"]))
	print("A fase 6 tem: " + str(Save.data["Fase1"]["nivel6"]["estrelas"]))

	if not Save.data["Fase1"]["nivel1"]["estrelas"] == 0:
		for i in Save.data["Fase1"]["nivel1"]["estrelas"]:
			get_node("Buttons/Nivel1-1/Sprite"+str(i+1)).modulate = Color("E8EB5D")
	
	if not Save.data["Fase1"]["nivel2"]["estrelas"] == 0:
		for i in Save.data["Fase1"]["nivel2"]["estrelas"]:
			get_node("Buttons/Nivel1-2/Sprite"+str(i+1)).modulate = Color("E8EB5D")
			
	if not Save.data["Fase1"]["nivel3"]["estrelas"] == 0:
		for i in Save.data["Fase1"]["nivel3"]["estrelas"]:
			get_node("Buttons/Nivel1-3/Sprite"+str(i+1)).modulate = Color("E8EB5D")
			
	if not Save.data["Fase1"]["nivel4"]["estrelas"] == 0:
		for i in Save.data["Fase1"]["nivel4"]["estrelas"]:
			get_node("Buttons/Nivel1-4/Sprite"+str(i+1)).modulate = Color("E8EB5D")
			
	if not Save.data["Fase1"]["nivel5"]["estrelas"] == 0:
		for i in Save.data["Fase1"]["nivel5"]["estrelas"]:
			get_node("Buttons/Nivel1-5/Sprite"+str(i+1)).modulate = Color("E8EB5D")
			
	if not Save.data["Fase1"]["nivel6"]["estrelas"] == 0:
		for i in Save.data["Fase1"]["nivel6"]["estrelas"]:
			get_node("Buttons/Nivel1-6/Sprite"+str(i+1)).modulate = Color("E8EB5D")




#Deslizar a tela para baixo e para cima
func _input(event):
	if event is InputEventScreenTouch:
		touch_pos = event.position.y
		
	if event is InputEventScreenDrag:
		if touch_pos - event.position.y >= 0 and $Buttons.rect_position.y > -1500:   #$Camera.position.y < 2521:
			$Buttons.rect_position.y -= 20
#			$Camera.position.y += 20
#			$TopSide.position.y += 20
			touch_pos = event.position.y
		elif touch_pos - event.position.y <= 0 and $Buttons.rect_position.y < 0: #$Camera.position.y > 750:
			$Buttons.rect_position.y += 20
#			$Camera.position.y -= 20 
#			$TopSide
			touch_pos = event.position.y
		else:
			pass


func _on_Back_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Fases.tscn")

func _on_Nivel11_pressed():
	scene = 1
	AudioController.Confirm()
	$AnimationPlayer.play("Transition")

func _on_Sound_pressed():
	AudioController.volume()

func _on_Nivel12_pressed():
	if Save.data["Fase1"]["nivel1"]["completo"] == false:
		$Alert.visible = true
	else:
		scene = 2
		AudioController.Confirm()
		$AnimationPlayer.play("Transition")

func _on_ConfirmButton_pressed():
	scene = 2
	AudioController.Confirm()
	$Alert.visible = false
	$AnimationPlayer.play("Transition")


func _on_CancelButton_pressed():
	$Alert.visible = false

func _on_Nivel13_pressed():
	if Save.data["Fase1"]["nivel2"]["completo"] == false:
		AudioController.blocked()
	else:
		scene = 3
		AudioController.Confirm()
		$AnimationPlayer.play("Transition")


func _on_Nivel14_pressed():
	if Save.data["Fase1"]["nivel3"]["completo"] == false:
		AudioController.blocked()
	else:
		scene = 4
		AudioController.Confirm()
		$AnimationPlayer.play("Transition")


func _on_Nivel15_pressed():
	if Save.data["Fase1"]["nivel4"]["completo"] == false:
		AudioController.blocked()
	else:
		scene = 5
		AudioController.Confirm()
		$AnimationPlayer.play("Transition")


func _on_Nivel16_pressed():
	if Save.data["Fase1"]["nivel5"]["completo"] == false:
		AudioController.blocked()
	else:
		scene = 6
		AudioController.Confirm()
		$AnimationPlayer.play("Transition")
