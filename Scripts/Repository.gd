extends Node2D


func _ready():
	$Buttons/Sound.pressed = AudioController.get_musicPlaying()


func _on_Back_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_Logica_pressed():
	pass


func _on_Variaveis_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/ContVariaveis.tscn")


func _on_Sound_pressed():
	AudioController.volume()
