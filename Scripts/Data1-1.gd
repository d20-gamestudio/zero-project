extends Node




var estrelas = 0
#var estrelas_guardadas = Save.data["Fase1"]["nivel1"]["estrelas"]
#onready var Fase_completa = false
onready var nivel_num = 1
onready var blocos_totais = 0
onready var Recpetor_activate = 1
onready var color_quant = 1 #Colocar a quantidade de cores da fase
export var solucao_perf = 4

func _ready():
	get_parent().get_node("Buttons").slider = false
	get_parent().get_node("Buttons").set_visibleBlocks(1)
	get_parent().get_node("Buttons").set_visibleTabs(color_quant)
	get_parent().get_node("Buttons").tab_num = color_quant
#	Save.data["nivel_atual"] = "1-1"
	Save.save_data()
	get_parent().get_node("Buttons").set_FaseText()

func _process(delta):
	pass



func _Win(): 
	Recpetor_activate -= 1
	if Recpetor_activate == 0:
		for i in get_parent().get_node("Buttons").comand_quant.size():
			blocos_totais += get_parent().get_node("Buttons").comand_quant[i] + 1
		$Win.visible = true
		_Estrelas(blocos_totais)
		


func _Estrelas(total):
	if blocos_totais <= solucao_perf:
#		Save.data["Fase1"]["nivel1"]["estrelas"] = 3
		estrelas = 3
		save()
		$Win/Feedback.text = "Excelente"
		$Win/Star0.modulate = Color("FFEA2D")
		$Win/Star1.modulate = Color("FFEA2D")
		$Win/Star2.modulate = Color("FFEA2D")
	elif blocos_totais > 3 and blocos_totais <= 5:
#		Save.data["Fase1"]["nivel1"]["estrelas"] = 2
		estrelas = 2
		save()
		$Win/Feedback.text = "Bom"
		$Win/Star0.modulate = Color("FFEA2D")
		$Win/Star1.modulate = Color("FFEA2D")
	elif blocos_totais > 5:
#		Save.data["Fase1"]["nivel1"]["estrelas"] = 1
		estrelas = 1
		save()
		$Win/Feedback.text = "Outra vez?"
		$Win/Star0.modulate = Color("FFEA2D")
	else:
		pass

func save():
	print(Save.data["Fase1"]["nivel1"]["completo"])
	
	if Save.data["Fase1"]["nivel1"]["completo"]:
		print("esse é o número de estrelas que ele deve ganhar: ", str(estrelas))
		print(Save.data["Fase1"]["nivel1"]["estrelas"])
#		if (estrelas < Save.data["Fase1"]["nivel1"]["estrelas"]):
#			print("O valor das estrelas é que ele perde é: ", str(Save.data["Fase1"]["nivel1"]["estrelas"] - estrelas))
#			Save.data["Fase1"]["estrelas_totais"] -= (Save.data["Fase1"]["nivel1"]["estrelas"] - estrelas)
#			Save.data["Fase1"]["nivel1"]["estrelas"] = estrelas
			
		if (estrelas > Save.data["Fase1"]["nivel1"]["estrelas"]):
			print("O valor das estrelas é que ele ganha é: ", str(estrelas - Save.data["Fase1"]["nivel1"]["estrelas"]))
			Save.data["Fase1"]["estrelas_totais"] += (estrelas - Save.data["Fase1"]["nivel1"]["estrelas"])
			Save.data["Fase1"]["nivel1"]["estrelas"] = estrelas
	else:
		Save.data["Fase1"]["nivel1"]["completo"] = true
		Save.data["Fase1"]["nivel1"]["anim1"] = true
		Save.data["Fase1"]["niveis_completos"] += 1
		Save.data["Fase1"]["estrelas_totais"] += estrelas
		Save.data["Fase1"]["nivel1"]["estrelas"] = estrelas
		Save.data["nivel_atual"] = "1-2"
		pass 
#	Save.data["Fase1"]["estrelas_totais"] += estrelas
	print("O jogador possui um total de: ", str(Save.data["Fase1"]["estrelas_totais"]), " estrelas")
	Save.save_data()

func set_receptorActivates():
	Recpetor_activate = 1
