extends Node2D

func _ready():
	$Fases/Sound.pressed = AudioController.get_musicPlaying()
	
	if Save.data["Fase1"]["nivel6"]["completo"] == true:
		$Fases/Variaveis.self_modulate = "#09684a"
	else: 
		pass
	
	if Save.data["Fase2"]["nivel6"]["completo"] == true:
		$Fases/Cond.self_modulate = "#09684a"
	else: 
		pass


func _on_Back_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_Logica_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/LogicaNiveis.tscn")


func _on_Variaveis_pressed():
	
	if Save.data["Fase1"]["nivel6"]["completo"] == true:
		AudioController.selected()
		get_tree().change_scene("res://Scenes/VarNiveis.tscn")
	else:
		AudioController.blocked()


func _on_Booleana_pressed():
	print(Save.data["Fase2"]["nivel6"]["completo"])
#	if Save.data["Fase2"]["nivel6"]["completo"] == true:
#		AudioController.selected()
#		get_tree().change_scene("res://Scenes/CondNiveis.tscn")
#	else:
	AudioController.blocked()

func _on_Repetio_pressed():
	AudioController.blocked()


func _on_Audio_pressed():
	AudioController.volume()


func _on_Cond_pressed():
	print(Save.data["Fase2"]["nivel6"]["completo"])
	if Save.data["Fase2"]["nivel6"]["completo"] == true:
		AudioController.selected()
		get_tree().change_scene("res://Scenes/CondNiveis.tscn")
	else:
		AudioController.blocked()

