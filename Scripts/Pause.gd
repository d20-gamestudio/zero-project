extends Node2D




#resume game
func _on_Resume_pressed():
	AudioController.selected()
	get_tree().paused = false
	visible = false
	
	

#back to home
func _on_Home_pressed():
	AudioController.selected()
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/Menu.tscn")


#open repository
func _on_Repository_pressed():
	AudioController.selected()
	get_tree().paused = false
	get_tree().change_scene("res://Scenes/Repository.tscn")


func _on_Close_pressed():
	AudioController.selected()
	get_tree().paused = false
	visible = false
