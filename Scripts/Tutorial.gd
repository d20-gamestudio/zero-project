extends Node2D

var dialogs = [
	"Bem vindo.\n \nNós somos a Organização Morath, o maior grupo de hackers do mundo.",
	"Você está recebendo essa mensagem pois foi um dos 64 escolhidos para participar do nosso teste:",
	"zerar a sequência de puzzles apresentados nesse jogo. Caso passe, se tornará um de nós.",
	"Lembre-se de que você precisa fazer er er er er \n \n %@%#%$%%%*^!%%$#$#",
	"................................",
	"ora ora... queM está aqui...",
	"Veio se alistar em M0rath, humanO?.",
	"Então desista!",
	"Se tinha alguma chance ela se foi, porque agora eu estou aqui...",
	"Meu nome é Zer0.",
	"Sou um vírus implantado para dificultar os seus desafios.",
	"Se ACHA que pode me derrotar, quero ver você tEntar!.",
	"Vou lançar dois desafios iniciais para ter certeza se é um oponente digno ou não.",
	"Se não for... vou crashar esse jogo!",
	"Adicione um ou mais bLocos à área de comandOs.",
#	Condições de erros 
	"LEntinho...Finalmente aprendeu. Vamos ao próximo desafio! Quero é ver você conseguir Esse...",
	"Escolha os blocos que irão conectar as coRes corretamente! Os que você escolheu antes não foram os corretos. Tsc... Tsc...",
	"LEntinho...Finalmente aprendeu. Vamos ao próximo desafio! Quero é ver você conseguir Esse...",
	"Escolha os blocos que irão conectar as coRes corretamente! Os que você escolheu antes não foram os corretos. Tsc... Tsc...",
	"GRRRR apressadinho não é? Já completou até a segunda tarefa: escolher os Blocos que conectam as cores de forma correta.",
	"TalvEz você me dê trabalho! AARRGGGG",
#	"Então... Boa [shake rate=20 level=10] Má [/shake] sorte!"
]

var control = 0
var finished = false
var exception = false
var finishDialog = false

func _ready():
	get_tree().paused = true
	$ZeroFaces.visible = false
	_load_dialog(0)

func _process(delta):
	$Continue.visible = finished


func _load_dialog(excpetion):
	if exception == true:
		$ColorRect2.color = "5B1718"
		$ColorRect2/ColorRect3.color = "AA3E3E"
		$Title.text = "Error.exe"
		match exception:
			0:
				$tutorial1/DialogText1.bbcode_text = "O jogo encontrou um error nele mesmo, ignore esta mensagem"
			1:
				$tutorial1/DialogText1.bbcode_text = "Error encontrado: em RUN e/ou End, apenas 1 por tipo deve ser usado no começo e fim do código"
			2:
				$tutorial1/DialogText1.bbcode_text = "Error encontrado: Blocos de comando, por favor coloque os blocos na ordem correta"
			3:
				$tutorial1/DialogText1.bbcode_text = "Error encontrado: Choque de cores na suas extremidades, por favor olhe com atenção seu código"
	else:
		if finishDialog == false:
			if control < dialogs.size() - 6:
				finished = false
				AudioController.Writing()
				print("Estou tocando")
				$tutorial1/DialogText1.percent_visible = 0
				$tutorial1/DialogText1.bbcode_text = dialogs[control]
				$ZeroFaces.frame = control - 4
				print(control)
				if control == 4:
					$ZeroFaces.visible = true
					$ColorRect2.color = "5B1718"
					$ColorRect2/ColorRect3.color = "AA3E3E"
					$tutorial1.rect_position.y = 800
				elif control == 3:
					$AnimationPlayer.play("ScreenBug")
				else:
					pass
					
				$Tween.interpolate_property($tutorial1/DialogText1, "percent_visible", 0, 1, 2,
				Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
				$Tween.start()
			else:
				AudioController.StopWriting()
				get_tree().paused = false
				queue_free()
			control+=1
		else:
			pass



func _on_TouchScreenButton_released():
	_load_dialog(0)
	

func _on_Tween_tween_all_completed():
	AudioController.StopWriting()
	finished = true


func _on_CloseTutorial_pressed():
	finishDialog = true
	AudioController.StopWriting()
	get_tree().paused = false
	queue_free()

