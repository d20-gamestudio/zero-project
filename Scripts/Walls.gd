extends TileMap


# Declare member variables here. Examples:
onready var half_cell_size = get_cell_size()/2
var direction = [Vector2(0,-1),Vector2(1,0),Vector2(0,1),Vector2(-1,0)]
var player_path = []

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func is_tile_vacant(pos, direction):
	var curr_tile = world_to_map(pos)
	var next_tile = get_cellv(curr_tile + direction)
	var next_tile_pos = Vector2()
	
	if(next_tile >= 01):
		next_tile_pos = map_to_world(curr_tile + direction) +  half_cell_size
		
	else:
		next_tile_pos = relocate(pos)
		
	return next_tile_pos

func relocate(pos):
	var tile = world_to_map(pos)
	return map_to_world(tile) + half_cell_size
	
func set_direction(pos, no_direction):
	var curr_tile = world_to_map(pos)
	for i in range(4):
		if i != no_direction:
			if get_cellv(curr_tile + direction[i]) == 01:
				return i
