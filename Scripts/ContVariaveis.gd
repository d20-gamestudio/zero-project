extends Node2D


var anim = 0

func _ready():
	$UI/Sound.pressed = AudioController.get_musicPlaying()


func _on_Text_pressed():
	if anim == 0:
		AudioController.ZoomIn()
		$AnimationPlayer.play("Grow")
		anim = 1
	else:
		AudioController.ZoomOut()
		$AnimationPlayer.play_backwards("Grow")
		anim = 0


func _on_Back_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Repository.tscn")


func _on_TextureButton_pressed():
	AudioController.volume()
