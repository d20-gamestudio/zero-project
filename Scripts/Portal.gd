extends Area2D


var id = 2
onready var direction = [$Dir/Up,$Dir/Right,$Dir/Down, $Dir/Left]
onready var rotation_d = [-90,0,90,-180]
export var portal = true
export var divide = true
export var up = false
export var right = false
export var down = false
export var left = false
onready var direction_vars = [up, right, down, left]

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in range(4):
		direction[i].visible = direction_vars[i]
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func reset():
	$Sprite.self_modulate = Color("#d4d4d4")
	for i in range(4):
		direction[i].visible = direction_vars[i]
		direction[i].rotation_degrees = rotation_d[i]
		direction[i].self_modulate = Color("#d4d4d4")
		
	
