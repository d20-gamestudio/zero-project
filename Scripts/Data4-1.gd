extends Node

onready var Recpetor_activate = 1
onready var Fase_completa = false
onready var nivel_num = 1
onready var blocos_totais = 0
onready var estrelas = 0
onready var color_quant = 2
var solucao_perf = 4

func _ready():
	#print(Recpetor_activate)
	get_parent().get_node("Buttons").slider = true	
	get_parent().get_node("Buttons").set_visibleBlocks(3)
	get_parent().get_node("Buttons").set_visibleTabs(color_quant)
	get_parent().get_node("Buttons").tab_num = color_quant
	Save.data["nivel_atual"] = "4-1"
	Save.save_data()
	get_parent().get_node("Buttons").set_FaseText()

func _Win(): 
	print(Recpetor_activate)
	Recpetor_activate -= 1
	if Recpetor_activate == 0:
		for i in get_parent().get_node("Buttons").comand_quant.size():
			blocos_totais += get_parent().get_node("Buttons").comand_quant[i] + 1
		$Win.visible = true
		_Estrelas(blocos_totais)
		
		

func _Estrelas(total):
	if blocos_totais <= solucao_perf:
		Save.data["Fase4"]["nivel1"]["estrelas"] = 3
		save()
		$Win/Feedback.text = "Excelente"
		$Win/Star0.modulate = Color("FFEA2D")
		$Win/Star1.modulate = Color("FFEA2D")
		$Win/Star2.modulate = Color("FFEA2D")
	elif blocos_totais > 4 and blocos_totais <= 6:
		Save.data["Fase4"]["nivel1"]["estrelas"] = 2
		save()
		$Win/Feedback.text = "Bom"
		$Win/Star0.modulate = Color("FFEA2D")
		$Win/Star1.modulate = Color("FFEA2D")
	elif blocos_totais > 6:
		Save.data["Fase4"]["nivel1"]["estrelas"] = 1
		save()
		$Win/Feedback.text = "Outra vez?"
		$Win/Star0.modulate = Color("FFEA2D")
	else:
		pass

func save():
	if Save.data["Fase4"]["nivel1"]["completo"] == true:
		pass
	else:
		Save.data["Fase4"]["nivel1"]["completo"] = true
		Save.data["Fase4"]["niveis_completos"] += 1
	Save.data["nivel_atual"] = "4-2"
	Save.data["prox_nivel"] = "4-2"
	Save.data["Fase4"]["estrelas_totais"] += estrelas
	#print(Save.data["Fase1"]["estrelas_totais"])
	Save.save_data()

func set_receptorActivates():
	Recpetor_activate = 2
