extends Area2D

var id = 3
export var key = false
export var blocked_final = -1
export var arrow = false
export var arrowL = -1
export var arrowR = -1
var colors = ["E47240", "F4DE67", "#E03132"]

# Called when the node enters the scene tree for the first time.
func _ready():
	if key == true:
		$Sprite/Key.visible = true
	elif arrow == true:
		$Sprite/Arrow_left.visible = true
		$Sprite/Arrow_left.modulate = Color(colors[arrowL])
		$Sprite/Arrow_right.visible = true
		$Sprite/Arrow_right.modulate = Color(colors[arrowR])

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
