extends Node2D

var touch_pos

func _ready():
	$UITop/Sound.pressed = AudioController.get_musicPlaying()

func _input(event):
	
	if event is InputEventScreenTouch:
		touch_pos = event.position.y
		
	if event is InputEventScreenDrag:
		if touch_pos - event.position.y >= 0 and $Credits.rect_position.y > -2500:   #$Camera.position.y < 2521:
			$Credits.rect_position.y -= 20
#			$Camera.position.y += 20
#			$TopSide.position.y += 20
			touch_pos = event.position.y
		elif touch_pos - event.position.y <= 0 and $Credits.rect_position.y < 5: #$Camera.position.y > 750:
			$Credits.rect_position.y += 20
#			$Camera.position.y -= 20 
#			$TopSide
			touch_pos = event.position.y
		else:
			pass


func _on_Back_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_TextureButton_pressed():
	AudioController.volume()

func _on_Zapsplat_pressed():
	OS.shell_open("https://www.zapsplat.com")


func _on_Kenney_pressed():
	OS.shell_open("https://www.kenney.nl/")


func _on_TouchScreenButton_pressed():
	pass
