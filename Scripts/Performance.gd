extends Node2D


var estrelas = [0,0,0]
var dourado = "#FFEA2D"



func _ready():
	$Buttons/Sound.pressed = AudioController.get_musicPlaying()
	_change_num1()

func _process(delta):
	pass

#change number of stars and fill progress bar
func _change_num1():
	print(Save.data["Fase1"]["estrelas_totais"])
	estrelas[0] = Save.data["Fase1"]["estrelas_totais"]
	
	if Save.data["Fase1"]["nivel1"]["completo"]:
		$Buttons/ProgressLogic/ProgLogic.text = str(int((Save.data["Fase1"]["niveis_completos"]/6.0)*100)) + "%"
		$Buttons/ProgressLogic/LogicBar.value = Save.data["Fase1"]["niveis_completos"]
		print(estrelas[0])
		if estrelas[0] == 18:
			$Buttons/ProgressLogic/Star1.modulate = dourado
			$Buttons/ProgressLogic/Star2.modulate = dourado
			$Buttons/ProgressLogic/Star3.modulate = dourado
		elif estrelas[0] > 12 and estrelas[0] < 17:
			$Buttons/ProgressLogic/Star1.modulate = dourado
			$Buttons/ProgressLogic/Star2.modulate = dourado
		else: #estrelas < 11:
			$Buttons/ProgressLogic/Star1.modulate = dourado
	else:
		pass
	_change_num2()
	

func _change_num2():
	#print(Save.data["Fase1"]["estrelas_totais"])
	estrelas[1] = Save.data["Fase2"]["estrelas_totais"]
	$Buttons/ProgressVar/ProgVar.text = str(int((Save.data["Fase2"]["niveis_completos"]/6.0)*100)) + "%"
	$Buttons/ProgressVar/VarBar.value = Save.data["Fase2"]["niveis_completos"]
	
	if Save.data["Fase2"]["nivel1"]["completo"]:
		if estrelas[1] == 18:
			$Buttons/ProgressVar/Star1.modulate = dourado
			$Buttons/ProgressVar/Star2.modulate = dourado
			$Buttons/ProgressVar/Star3.modulate = dourado
		elif estrelas[1] > 12 and estrelas[1] < 17:
			$Buttons/ProgressVar/Star1.modulate = dourado
			$Buttons/ProgressVar/Star2.modulate = dourado
		else: #estrelas < 11:
			$Buttons/ProgressVar/Star1.modulate = dourado
	else:
		pass
	_change_num3()

func _change_num3():
	#print(Save.data["Fase1"]["estrelas_totais"])
	estrelas[2] = Save.data["Fase3"]["estrelas_totais"]
	$Buttons/ProgressBool/ProgBool.text = str(int((Save.data["Fase3"]["niveis_completos"]/6.0)*100)) + "%"
	$Buttons/ProgressBool/BoolBar.value = Save.data["Fase3"]["niveis_completos"]
	
	if Save.data["Fase3"]["nivel1"]["completo"]:
		if estrelas[2] == 18:
			$Buttons/ProgressBool/Star1.modulate = dourado
			$Buttons/ProgressBool/Star2.modulate = dourado
			$Buttons/ProgressBool/Star3.modulate = dourado
		elif estrelas[2] > 12 and estrelas[2] < 17:
			$Buttons/ProgressBool/Star1.modulate = dourado
			$Buttons/ProgressBool/Star2.modulate = dourado
		else:
			$Buttons/ProgressBool/Star1.modulate = dourado
	else:
		pass




func _on_Back_pressed():
	AudioController.selected()
	get_tree().change_scene("res://Scenes/Menu.tscn")


func _on_TextureButton_pressed():
	AudioController.volume()
