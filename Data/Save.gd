extends Node


var path = "res://Data/Save.json"

#data["Fase1"]["nivel1"]["estrelas"]
#custom data
var data = {
	"nivel_atual": "1-1",
	"prox_nivel": "1-2",
	"Fase1": {
		"first_anim": false,
		"niveis_completos": 0,
		"estrelas_totais": 0,
		"nivel1": {
			"completo": false,
			"estrelas": 0
			},
		"nivel2": {
			"completo": false,
			"estrelas": 0
			},
		"nivel3": {
			"completo": false,
			"estrelas": 0
			},
		"nivel4": {
			"completo": false,
			"estrelas": 0
			},
		"nivel5": {
			"completo": false,
			"estrelas": 0
		},
		"nivel6": {
			"completo": false,
			"estrelas": 0
		}
	},
	"Fase2": {
		"Fase": "Fase2-",
		"first_anim": false,
		"last_anim": "Trans2-2",
		"niveis_completos": 0,
		"estrelas_totais": 0,
		"nivel1": {
			"completo": false,
			"estrelas": 0
			},
		"nivel2": {
			"completo": false,
			"estrelas": 0
			},
		"nivel3": {
			"completo": false,
			"estrelas": 0
			},
		"nivel4": {
			"completo": false,
			"estrelas": 0
			},
		"nivel5": {
			"completo": false,
			"estrelas": 0
		},
		"nivel6": {
			"completo": false,
			"estrelas": 0
		}
	},
	"Fase3": {
		"Fase": "Fase3-",
		"first_anim": false,
		"niveis_completos": 0,
		"estrelas_totais": 0,
		"nivel1": {
			"completo": false,
			"estrelas": 0
			},
		"nivel2": {
			"completo": false,
			"estrelas": 0
			},
		"nivel3": {
			"completo": false,
			"estrelas": 0
			},
		"nivel4": {
			"completo": false,
			"estrelas": 0
			},
		"nivel5": {
			"completo": false,
			"estrelas": 0
		},
		"nivel6": {
			"completo": false,
			"estrelas": 0
		}
	},
	"Fase4": {
		"Fase": "Fase4-",
		"first_anim": false,
		"niveis_completos": 0,
		"estrelas_totais": 0,
		"nivel1": {
			"completo": false,
			"estrelas": 0
			},
		"nivel2": {
			"completo": false,
			"estrelas": 0
			},
		"nivel3": {
			"completo": false,
			"estrelas": 0
			},
		"nivel4": {
			"completo": false,
			"estrelas": 0
			},
		"nivel5": {
			"completo": false,
			"estrelas": 0
		},
		"nivel6": {
			"completo": false,
			"estrelas": 0
		}
	},
}




#create data file
func _ready():
	var valid_loaded = load_data()
		
	if valid_loaded == false:
		save_data()


#save the progress when called
func save_data():
	var file = File.new()
	file.open(path, File.WRITE)
	file.store_line(to_json(data))
#	file.store_string("banana")
	file.close()
	

#load progress when called
func load_data():
	var file = File.new()
	if not file.file_exists(path):
		return false
	
	file.open(path, file.READ)
	var text = file.get_as_text()
	data = parse_json(text)
	file.close()
	return true

